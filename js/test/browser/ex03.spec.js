const test = require('ava');

function scaffoldStructure(document, data) {
  let ul = document.createElement('ul');
  data.forEach(item => {
    let li = document.createElement('li');
    li.innerHTML = `<strong>${item.name}</strong>-${item.email}`;
    li.classList.add('name');
    ul.append(li);
  });
  document.body.appendChild(ul);
}

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);
  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length);
});
