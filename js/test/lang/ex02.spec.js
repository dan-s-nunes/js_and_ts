function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  beforeEach(() => {
    name = 'Daniel';
    email = 'daniel@mail.com.br';
    password = 'mac0475';
    passwordConfirmation = 'mac0475';
  });

  describe('invalid user name', () => {
    beforeAll(() => {
      nameSizeError = new Error('validation error: invalid name size');
    });
    test('name too short', () => {
      const shortNameEnv = () => {
        name = '';
        createUser(name, email, password, passwordConfirmation);
      };
      expect(shortNameEnv).toThrow(nameSizeError);
    });

    test('name too long', () => {
      const longNameEnv = () => {
        name = 'Daniel'.repeat(11);
        createUser(name, email, password, passwordConfirmation);
      };
      expect(longNameEnv).toThrow(nameSizeError);
    });
  });

  describe('invalid user email', () => {
    test('invalid format', () => {
      const invalidEmailEnv = () => {
        email = 'daniel@mail.br';
        createUser(name, email, password, passwordConfirmation);
      };
      expect(invalidEmailEnv).toThrow(
        new Error('validation error: invalid email format')
      );
    });
  });

  describe('invalid user password', () => {
    beforeAll(() => {
      passwordError = new Error('validation error: invalid password format');
    });
    test('contains invalid characters', () => {
      const invalidCharEnv = () => {
        password = '13@/]-x2k)(*)%';
        createUser(name, email, password, passwordConfirmation);
      };
      expect(invalidCharEnv).toThrow(passwordError);
    });

    test('password is too short', () => {
      const shortPasswordEnv = () => {
        password = '1234';
        createUser(name, email, password, passwordConfirmation);
      };
      expect(shortPasswordEnv).toThrow(passwordError);
    });
  });

  describe('invalid password confirmation', () => {
    beforeAll(() => {
      passwordConfirmationError = new Error(
        'validation error: confirmation does not match'
      );
    });

    test("password and confirmation don't match", () => {
      const unmatchEnv = () => {
        passwordConfirmation = 'mac0472';
        createUser(name, email, password, passwordConfirmation);
      };
      expect(unmatchEnv).toThrow(passwordConfirmationError);
    });
  });
});
